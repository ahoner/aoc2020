import { readInput } from '../Utils.ts';

const text = await readInput('6');
const input = text.split('\n\n').filter(line => line !== '').map(line => line.split('\n').filter(line => line !== ''));

const sumMap = input.map(group => {
  const set = new Set<string>();
  group.forEach(answers => answers.split('').forEach(answer => set.add(answer)))
  return set.size;
})

const sum = sumMap.reduce((acc, curr) => acc += curr, 0);
console.log(`Part 1 : ${sum}`);

const allSumMap = input.map(group => {
  const map = new Map<string, number>();
  group.forEach(answers => answers.split('').forEach(answer => {
    const value = map.get(answer);
    if(value !== undefined)
      map.set(answer, value + 1);
    else
      map.set(answer, 1);
  }))

  const unan = Array.from(map.values()).filter(sum => sum === group.length).length;
  return unan
})

const unanSum = allSumMap.reduce((acc, curr) => acc += curr, 0);
console.log(`Part 2 : ${unanSum}`);
