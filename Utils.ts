import { readJsonSync } from 'https://deno.land/x/jsonfile/mod.ts';
const cookie = readJsonSync('./cookie.json') as Cookie;

export async function readInput(day: string): Promise<string> {
  const url = `https://adventofcode.com/2020/day/${day}/input`;
  const res = await fetch(url, {
    headers: {
      cookie: cookie.cookie
    }
  });
  return (await res.text());
}

export function hasSum(sum: number, arr: number[]): number {
  return arr.map(num => sum - num) .filter(num => arr.includes(num))
  .filter(num => num !== 0)[0];
}

interface Cookie {
  cookie: string
}