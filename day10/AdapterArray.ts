import  { readInput } from '../Utils.ts';

const text = await readInput('10');
const input = text.split('\n').filter(line => line !== '').map(line => Number.parseInt(line));
const sorted = input.sort((a, b) => a - b);
sorted.unshift(0);
sorted.push(sorted[sorted.length - 1] + 3)

console.log(`Part 1 : ${part1(sorted)}`);
console.log(`Part 2 : ${getArrangements(sorted)}`);

function part1(arr: number[]) {
  let curr = 0;
  let sum1 = 0;
  let sum3 = 0;
  sorted.forEach((jolt) => {
    const diff = jolt - curr;
    switch (diff) {
      case 1:
        sum1++;
        break;

      case 3:
        sum3++
        break;

    }
    curr = jolt;
  });
  return sum1 * sum3;
}

function getArrangements(arr: number[]) {
  const arrArr = [1];
  arr.forEach((jolt, i) => {
    if (arrArr[i] === undefined)
      arrArr[i] = 0;

    for(let j = i - 3; j < i; j++) {
      if (j >= 0 && jolt - arr[j] <= 3)
        arrArr[i] += arrArr[j];
    }
  })
  return arrArr[arrArr.length - 1];
}