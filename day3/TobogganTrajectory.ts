import { readInput } from '../Utils.ts';

const snow = '.';
const tree = '#';

const text = await readInput('3');
const map = text.split('\n').filter(line => line !== '');
const height = map.length;
const width = map[0].length;
const start: Pos = {
  x: 0,
  y: 0,
};
const slope: Vel = {
  x: 3,
  y: 1,
};
const part2Slopes: Vel[] = [{
    x: 1,
    y: 1,
  }, {
    x: 3,
    y: 1
  }, {
    x: 5,
    y: 1
  }, {
    x: 7,
    y: 1
  }, {
    x: 1,
    y: 2
  }
];

console.log(`Part 1 : trees hit ${sledBoi(start, slope, map)}`);
const part2Solutions = part2Slopes.map(part2Slope => sledBoi(start, part2Slope, map));
console.log(`Part 2 : ${part2Solutions.reduce((acc, curr) => acc * curr, 1)}`);

function sledBoi(start: Pos, speed: Vel, map: string[]): number {
  const nextPos: Pos = {
    x: start.x + speed.x,
    y: start.y + speed.y,
  }
  if (nextPos.y >= height)
    return 0;

  if (map[nextPos.y][nextPos.x % width] === tree)
    return 1 + sledBoi(nextPos, speed, map);

  return sledBoi(nextPos, speed, map);
}

interface Vel {
  x: number;
  y: number;
}

interface Pos {
  x: number,
  y: number
}