import { readInput } from '../Utils.ts';

const text = await readInput('12');
const input = text.split('\n').filter(line => line !== '');

console.log(`Part 1 : ${manhattanDistance(input)}`);
console.log(`Part 2 : ${wayPointTravel(input)}`);

function manhattanDistance(input: string[]): number {
  let bearing = 'E';
  const distSums: ManhattenDistance = {
    vert: 0,
    horz: 0
  }
  input.forEach(line => {
    const match = /^([A-Z])(\d+)$/.exec(line);
    if (!match)
      throw new Error('Invalid line');

    const [fullLine, dir, dist] = match;
    switch (dir) {
      case 'F':
        updatePos(bearing, Number.parseInt(dist), distSums);
        break;

      case 'R':
        bearing = turn(bearing, dir, Number.parseInt(dist))
        break;

      case 'L':
        bearing = turn(bearing, dir, Number.parseInt(dist))
        break;

      default:
        updatePos(dir, Number.parseInt(dist), distSums);
        break;
    }
  });
  return Math.abs(distSums.horz) + Math.abs(distSums.vert);
}

function updatePos(bearing: string, dist: number, distSums: ManhattenDistance): void {
  switch (bearing) {
    case "N":
      distSums.vert += dist;
      break;

    case "S":
      distSums.vert -= dist;
      break;

    case "E":
      distSums.horz += dist;
      break;

    case "W":
      distSums.horz -= dist;
      break;

    default:
      break;
  }
}

function turn(bearing: string, dir: string, dergees: number): string {
  const bearingMap = new Map<string, number>([
    ['N', 0],
    ['E', 90],
    ['S', 180],
    ['W', 270]
  ]);
  const bearingMapReverse = new Map<number, string>([
    [0, 'N'],
    [90, 'E'],
    [180, 'S'],
    [270, 'W']
  ]);
  let bearingAsDeg = bearingMap.get(bearing);
  if (bearingAsDeg === undefined)
    throw new Error('Invalid bearing');

  if (dir === 'R')
    bearingAsDeg += dergees;
  else
    bearingAsDeg -= dergees;

  if (bearingAsDeg < 0)
    bearingAsDeg += 360;
  else if (bearingAsDeg >= 360)
    bearingAsDeg -= 360;

  const result = bearingMapReverse.get(bearingAsDeg);
  if (result !== undefined)
    return result

  throw new Error('Invalid bearing');
}

function wayPointTravel(input: string[]): number {
  let distSums: ManhattenDistance = {
    vert: 0,
    horz: 0
  };
  let wayPoint: ManhattenDistance = {
    vert: 1,
    horz: 10
  }
  input.forEach(line => {
    const match = /^([A-Z])(\d+)$/.exec(line);
    if (!match)
      throw new Error('Invalid line');

    const [fullLine, dir, dist] = match;
    if (dir === 'F') {
      distSums.vert += wayPoint.vert * Number.parseInt(dist);
      distSums.horz += wayPoint.horz * Number.parseInt(dist);
    } else {
      updateWaypoint(wayPoint, dir, Number.parseInt(dist));
    }
  })
  return Math.abs(distSums.horz) + Math.abs(distSums.vert);
}

function updateWaypoint(wayPoint: ManhattenDistance, dir: string, dist: number): void {
  switch (dir) {
    case 'N':
      wayPoint.vert += dist;
      break;

    case 'S':
      wayPoint.vert -= dist;
      break;

    case 'E':
      wayPoint.horz += dist;
      break;

    case 'W':
      wayPoint.horz -= dist;
      break;

    default:
      rotateWaypoint(wayPoint, dir, dist);
      break;
  }
}

function rotateWaypoint(wayPoint: ManhattenDistance, dir: string, degrees: number): void {
  const x = wayPoint.horz, y = wayPoint.vert;
  const rads = dir === 'R' ? degrees * Math.PI / 180 * -1 : degrees * Math.PI / 180;
  wayPoint.horz = Math.round(Math.cos(rads) * x - Math.sin(rads) * y);
  wayPoint.vert = Math.round(Math.sin(rads) * x + Math.cos(rads) * y);
}

interface ManhattenDistance {
  vert: number,
  horz: number
}