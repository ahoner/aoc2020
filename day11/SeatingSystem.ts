import { readInput } from '../Utils.ts';

const text = await readInput('11');
const input = text.split('\n').filter(line => line !== '').map(line => line.split(''));

const totalSeatsPart1 = countOccupied(run(input, updateSeatPart1));
console.log(`Part 1 : ${totalSeatsPart1}`);
const totalSeatsPart2 = countOccupied(run(input, updateSeatPart2));
console.log(`Part 2 : ${totalSeatsPart2}`);

function run(arr: string[][], updater: (x: number, y: number, array: string[][]) => string) {
  let stable = false;
  let curr = arr;
  while(!stable) {
    let prev = curr;
    curr = updateAllSeats(curr, updater);
    stable = JSON.stringify(prev) === JSON.stringify(curr);
  }
  return curr;
}

function updateAllSeats(arr: string[][], updater: (x: number, y: number, array: string[][]) => string) {
  let curr: string[][] = [];
  for (let i = 0; i < arr.length; i++) {
    const line = [];
    for (let j = 0; j < arr[i].length; j++) {
      const char = updater(j, i, arr);
      line.push(char);
    }
    curr.push(line);
  }
  return curr;
}

function updateSeatPart1(x: number, y: number, arr: string[][]): string {
  const empty = "L", occ = "#";
  const char = arr[y][x];
  let numOcc = 0;
  for (let i = y - 1; i <= y + 1; i++) {
    if (i >= 0 && i < arr.length) {
      for (let j = x - 1; j <= x + 1; j++) {
        const curr = arr[i][j];
        if (j >= 0 && j < arr[i].length
            && curr === occ
            && !(y === i && x === j)) {
          numOcc++;
        }
      }
    }
  }
  if (char === empty && numOcc === 0) {
    return occ;
  } else if (char === occ && numOcc >= 4) {
    return empty;
  }
  return char;
}

function updateSeatPart2(x: number, y: number, arr: string[][]): string {
  const empty = "L", occ = "#";
  const char = arr[y][x];
  let numOcc = 0;
  for (let vx = -1; vx < 2; vx++) {
    for (let vy = -1; vy < 2; vy++) {
      const inVision = castRay(x, y, vx, vy, arr);
      if (inVision === occ)
        numOcc++;
    }
  }
  if (char === empty && numOcc === 0) {
    return occ;
  } else if (char === occ && numOcc >= 5) {
    return empty;
  }
  return char;
}

function castRay(x: number, y: number, vx: number, vy: number, arr: string[][]): string {
  const empty = 'L', occ = '#', flr = '.';
  let currX = x + vx, currY = y + vy;
  let inBounds = currY >= 0 && currY < arr.length && currX >= 0 && currX < arr[0].length;
  if (vx === 0 && vy === 0)
    return 'L';

  while (inBounds) {
    const char = arr[currY][currX];
    if (char !== flr) 
      return char;

    currX += vx, currY+= vy;
    inBounds = currY >= 0 && currY < arr.length && currX >= 0 && currX < arr[0].length;
  }
  return 'L';
}

function countOccupied(arr: string[][]): number {
  return arr.reduce((acc, curr) => {
    const currSum = curr.reduce((acc2, curr2) => {
      return acc2 + (curr2 === "#" ? 1 : 0);
    }, 0);
    return acc + currSum;
  }, 0);
}
