import { readInput } from '../Utils.ts';

const text = await readInput('7');
const input = text.split('\n').filter(line => line !== '');

const startBags: string[] = []; 
const rules: Map<string, string[]> = new Map();

input.forEach(line => {
  const args = line.split('s contain ');
  const parentBag = args[0].split(' ');
  const parentBagAdj = parentBag[0] + ' ' + parentBag[1];
  const childernBags = args[1].split(', ');
  const childernBagsAdj = childernBags.map(bag => {
    const wordArr = bag.split(' ');
    return wordArr[0] + ' ' + wordArr[1] + ' ' + wordArr[2];
  })
  startBags.push(parentBagAdj)
  rules.set(parentBagAdj, childernBagsAdj);
});
const containsShinyGold = startBags.map(bag => reduceBag(bag)).filter(bool => bool === true).length;
console.log(`Part 1 : ${containsShinyGold}`);
const numInShinyGold = countInternalBags('shiny gold');
console.log(`Part 2 : ${numInShinyGold}`)

function reduceBag(bag: string): boolean {
  const bagRules = rules.get(bag);
  if (bagRules === undefined)
    return false;

  if (bagRules.map(bagRule => bagRule.includes('shiny gold')).includes(true))
    return true;

  return bagRules.map(childBag => {
    const childBagArgs = childBag.split(' ');
    return reduceBag(childBagArgs[1] + ' ' + childBagArgs[2]);
  }).includes(true);
}

function countInternalBags(bag: string): number {
  const bagRules = rules.get(bag);
  if (bagRules === undefined)
    return 0;
  
  const bagSums = bagRules.map(childBag => {
    if (childBag === 'no other bags.')
      return 0;

    const childBagArgs = childBag.split(' ');
    const numBags = Number.parseInt(childBagArgs[0]);
    const bagName = childBagArgs[1] + ' ' + childBagArgs[2];
    return numBags + numBags * countInternalBags(bagName);
  })
  return bagSums.reduce((acc, curr) => acc += curr, 0);
}