import { readInput } from '../Utils.ts';

const text = await readInput('5');
const input = text.split('\n').filter(line => line !== '');

let highestId = 0;
input.forEach(seat => {
  const seatId = getSeatId(seat);
  highestId = seatId > highestId ? seatId : highestId;
})
console.log(`Part 1 : ${highestId}`)

const seatIds = input
  .map(dirs => getSeatId(dirs))
  .sort((a, b) => a - b);
let part2Solution = 0;
let prevId = seatIds[0];
seatIds.forEach(seatId => {
  if (prevId !== seatId && seatId - prevId !== 1)
    part2Solution = seatId - 1;
  else (prevId !== seatId && seatId - prevId === 1)
    prevId = seatId;
});
console.log(`Part 2 : ${part2Solution}`);

function binarySearch(directions: string[], low: number, high: number): number {
  const dir = directions.shift();
  const midPoint = (high - low) / 2;
  switch (dir) {
    case "F":
    case "L":
      if (directions.length === 0) {
        return low;
      } else {
        const newHigh = low + Math.floor(midPoint);
        return binarySearch(directions, low, newHigh);
      }

    case "B":
    case "R":
      if (directions.length === 0) {
        return high;
      } else {
        const newLow = low + Math.ceil(midPoint);
        return binarySearch(directions, newLow, high);
      }

    default:
      throw new Error('Invalid list input');
  }
}

function getSeatId(dirs: string) {
  const rowLow = 0, rowHigh = 127;
  const colLow = 0, colHigh = 7;

  const rowDirs = dirs.substring(0, dirs.length - 3).split('');
  const colDirs = dirs.substring(dirs.length - 3).split('');
  const rowNum = binarySearch(rowDirs, rowLow, rowHigh);
  const colNum = binarySearch(colDirs, colLow, colHigh);
  return rowNum * 8 + colNum;
}