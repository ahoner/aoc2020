import { readInput } from '../Utils.ts';

const text = await readInput('8');
const input = text.split('\n').filter(line => line !== '');

try {
  const acc = execute(0, 0, [], input);
  console.log(acc);
} catch (e) {
  console.log(e)
}

try {
  const acc = fix(input);
  console.log(acc)
} catch (e) {
  console.log(e);
}

function execute(line: number, acc: number, executedLines: number[], commands: string[]): number {
  if (line === commands.length)
    return acc;

  if (executedLines.includes(line))
    throw new Error(`Infinite loop detected, re-read on line : ${line}, acc : ${acc}`);

  executedLines.push(line);
  const args = commands[line].split(' ');
  const op = args[0];
  const arg = args[1];
  switch (op) {
    case 'acc':
      return execute(line + 1, acc + Number.parseInt(arg), executedLines, commands);

    case 'jmp':
      return execute(line + Number.parseInt(arg), acc, executedLines, commands);

    case 'nop':
      return execute(line + 1, acc, executedLines, commands);

    default:
      throw new Error('invalid operation');

  }
}

function fix(commands: string[]) {
  let line = 0;
  let fixed = false;

  while(!fixed) {
    const inputCopy: string[] = [];
    commands.forEach(command => inputCopy.push(command));
    const args = inputCopy[line].split(' ');
    const op = args[0];
    const arg = args[1];

    switch(op) {
      case 'acc':
        line++;
        break;
      case 'jmp':
        inputCopy[line] = 'nop ' + arg;
        try {
          return execute(0, 0, [], inputCopy);
        } catch {
          line++
        }
        break;

      case 'nop':
        inputCopy[line] = 'jmp ' + arg;
        try {
          return execute(0, 0, [], inputCopy);
        } catch {
          line++
        }
        break;
    }
  }
}