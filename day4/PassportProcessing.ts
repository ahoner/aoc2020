import { readInput } from '../Utils.ts';

const text = await readInput('4');

const input = text.split('\n\n').map(line => line.replaceAll('\n', ' '));

let validPassports1 = 0;
input.forEach(line => {
  const isValidPassport = checkField(line);
  if (isValidPassport)
    validPassports1++;
});

console.log(`Solution 1 : ${validPassports1}`);

let validPassports2 = 0;
input.forEach(line => {
  const validMap = line.split(' ').filter(field => field !== '').map(field => validateField(field));
  const allValid = !validMap.includes(false) && checkField(line);
  if (allValid)
    validPassports2++;
})

console.log(`Solution 2 : ${validPassports2}`);

function checkField(line: string) {
  const regex = /byr:|iyr:|eyr:|hgt:|hcl:|ecl:|pid:/g;
  const results = Array.from(line.matchAll(regex));
  return results.length === 7;
}

function validateField(field: string) {
  const fieldArr = field.split(':');
  const name = fieldArr[0];
  const value = fieldArr[1];
  switch (name) {
    case 'byr':
      return inBound(Number.parseInt(value), 1920, 2002);

    case 'iyr':
      return inBound(Number.parseInt(value), 2010, 2020);

    case 'eyr':
      return inBound(Number.parseInt(value), 2020, 2030);

    case 'hgt':
      const height = value.substring(0, value.length - 2);
      const units = value.substring(value.length - 2);
      return validHeight(Number.parseInt(height), units);

    case 'hcl':
      return validHairColor(value);

    case 'ecl':
      return validEyeColor(value);

    case 'pid':
      return validPid(value);

    case 'cid':
      return true;

    default: 
      return false;

  }
}

function inBound(num: number, low: number, high: number) {
  return num >= low && num <= high;
}

function validHeight(height: number, units: string) {
  switch (units) {
    case 'cm': 
      return inBound(height, 150, 193);
    
    case 'in':
      return inBound(height, 59, 76);

    default:
      return false;
  }
}

function validHairColor(color: string) {
  const regex = /\#[a-f0-9]+/g;
  return color.length === 7 && color.match(regex)?.length !== 0;
}

function validEyeColor(ecl: string) {
  const validColors = ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth'];
  return validColors.includes(ecl);
}

function validPid(pid: string) {
  const regex = /[0-9]+/g;
  return pid.length === 9 && pid.match(regex)?.length !== 0;
}