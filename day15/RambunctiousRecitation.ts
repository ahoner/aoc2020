import { readInput } from '../Utils.ts';

function memoryGamePart1(input: number[], depth: number) {
  const ageMap = new Map<number, number>();
  const seenBefore = new Set<number>();
  input.forEach((n, i) => {
    if (i < input.length - 1) {
      seenBefore.add(n);
    }
    ageMap.set(n, i + 1);
  });
  let i = input.length;
  let lastSpoken = input[input.length - 1];
  while(i < depth) {
    if (!seenBefore.has(lastSpoken)) {
      seenBefore.add(lastSpoken)
      ageMap.set(lastSpoken, i);
      lastSpoken = 0;
    } else {
      const tmp = ageMap.get(lastSpoken);
      if (tmp === undefined)
        throw new Error('invalid key')

      ageMap.set(lastSpoken, i)
      lastSpoken = i - tmp;
    }
    i++
  }
  console.log(lastSpoken)
}

if (import.meta.main) {
  const text = await readInput('15');
  const input = text.split(',')
    .map(n => Number.parseInt(n));

  memoryGamePart1(input, 2020);
  memoryGamePart1(input, 30000000);
}