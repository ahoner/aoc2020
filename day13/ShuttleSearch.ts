import { readInput } from '../Utils.ts';

const text = await readInput('13');
const [timestamp, line] = text.split('\n').filter(line => line !== '');
const buses = line.split(',').filter(bus => bus !== 'x').map(id => Number.parseInt(id));

part1(Number.parseInt(timestamp), buses);
part2(line);

function part1(timestamp: number, buses: number[]) {
  const tuple = closestMultiple(timestamp, buses);
  console.log(`Part 1 : ${(tuple.mult - timestamp) * tuple.id}`)
}

function closestMultiple(timestamp: number, buses: number[]): Tuple {
  let current = timestamp;
  let notFound = true;
  let ret: Tuple = {
    mult: 0,
    id: 0
  };
  while(notFound) {
    buses.forEach(bus => {
      if (current % bus === 0) {
        ret = {
          mult: current,
          id: bus
        }
        notFound = false;
        return;
      }
    });
    current++;
  }
  return ret;
};

function part2(line: string) {
  const ids = line.split(',').map(str => {
    if (str === 'x')
      return -1

    return Number.parseInt(str)
  });
  const a: number[] = [0];
  ids.forEach((id, i) => {
    if (id !== -1 && i !== 0) {
      a.push(id - i);
    }
  })
  const solution = crt(a, ids.filter(id => id !== -1))
  console.log(`Part 2 : ${solution}`);
}

function crt(a: number[], m: number[]) {
  let M = 1;
  m.forEach(i => M *= i)
  const Mi = m.map(i => Math.floor(M / i));
  const yi = m.map((curr, i) => inverse(Mi[i], curr));
  const X = yi.map((curr, i) => a[i] * Mi[i] * curr).reduce((acc, num) => acc + num, 0);
  return X % M;
}

function inverse(n: number, m: number) {
  for (let i = 1; i < m; i++)
    if (n * i % m === 1)
     return i;

  return Number.NaN;
}

interface Tuple {
  mult: number,
  id: number
}