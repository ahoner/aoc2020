import { readInput, hasSum } from '../Utils.ts';

const text = await readInput('9');
const input = text.split('\n').filter(line => line !== '').map(str => Number.parseInt(str));
const startNums = input.slice(0, 25);

const num = xmasAttack(25, input.slice(0, 25), 26, input);
console.log(`Part 1 : ${num}`);
const arr = contiguousSum(num, input)?.sort();
if (arr)
  console.log(`Part 2 : ${arr[0] + arr[arr.length - 1]}`);

function xmasAttack(prevNumSize: number, prevNums: number[], index: number, fullList: number[]): number {
  const current = fullList[index];
  const valid = hasSum(current, prevNums);
  if (!valid)
    return current;

  const newArr = fullList.slice(index - prevNumSize, index + 1);
  return xmasAttack(prevNumSize, newArr, index + 1, fullList);
}

function contiguousSum(sum: number, arr: number[]) {
  let low = 0, high = 1;
  let found = false;
  while(!found) {
    const subArr = arr.slice(low, high);
    const currSum = sumArr(subArr);
    if (currSum < sum)
      high++
    else if (currSum > sum)
      low++
    else
      return subArr
  }
}

function sumArr(arr: number[]) {
  return arr.reduce((acc, curr) => acc + curr, 0);
}