import { readInput } from '../Utils.ts';

const text = await readInput('1');
const input = text.split('\n').filter(line => line !== undefined);
const report = input.map(line => Number.parseInt(line));
const target = 2020;

const solution1num1 = hasSum(target, report);
const solution1num2 = target - solution1num1
console.log(`Part 1 solution: ${solution1num1 * solution1num2}`);

const diffMap = report.map(num => target - num);
const trioMap = diffMap
.map(num => hasSum(num, report))
.filter(num => num !== undefined);
const filteredTrioMap = trioMap.filter((num, i) => trioMap.indexOf(num) === i);
const solution2num1 = filteredTrioMap[0];
const solution2num2 = filteredTrioMap[1];
const solution2num3 = target - solution2num1 - solution2num2;
console.log(`Part 2 solution: ${solution2num1 * solution2num2 * solution2num3}`);

export function hasSum(sum: number, arr: number[]): number {
  return arr.map(num => sum - num) .filter(num => arr.includes(num))
  .filter(num => num !== 0)[0];
}