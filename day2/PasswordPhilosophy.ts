import { readInput } from '../Utils.ts';

const text = await readInput('2');
const lines = text.split('\n').filter(line => line !== '');

const numValidPasswords1 = lines.filter(line => {
  const { min, max, letter, password } = parseLine(line);
  return letterTotalInRange(min, max, letter, password);
}).length;
const numValidPasswords2 = lines.filter(line => {
  const { min, max, letter, password } = parseLine(line);
  return letterInPositions(min, max, letter, password);
}).length;

console.log(`Solution 1 : ${numValidPasswords1}`)
console.log(`Solution 2 : ${numValidPasswords2}`)

function parseLine(line: string): lineParams {
  const args = line.split(' ');
  const range = args[0].split('-');
  console.log(args)
  return {
    min: Number.parseInt(range[0]),
    max: Number.parseInt(range[1]),
    letter: args[1][0],
    password: args[2],
  };
}

function letterTotalInRange(min: number, max: number, letter: string, password: string): boolean {
  const initialLength = password.length;
  const finalLength = password.split('').filter(passwordLetter => passwordLetter !== letter).length; 
  const diff = initialLength - finalLength;
  return diff >= min && diff <= max;
}

function letterInPositions(low: number, high: number, letter: string, password: string): boolean {
  return ((letter === password[low - 1] || letter === password[high - 1]) && !(password[low - 1] === password[high - 1]));
}

interface lineParams {
  min: number,
  max: number,
  letter: string,
  password: string
};