import { readInput } from '../Utils.ts';

function initializeProgram(program: string[]) {
  const memoryMap = new Map<number, string>();
  let mask = '';
  program.forEach(line => {
    const [, op, ...args] = Array.from(praseLine(line));
    switch(op) {
      case 'mask':
        mask = args[0];
        break;

      case 'mem':
        const result = applyMask(mask, Number.parseInt(args[1]));
        memoryMap.set(Number.parseInt(args[0]), result);
        break;

      default:
        throw new Error('Invalid Operation');
    }
  });
  return Array.from(memoryMap.values());
}

function initializeWithFloats(program: string[]) {
  const memoryMap = new Map<number, string>();
  let mask = '';
  program.forEach(line => {
    const [, op, ...args] = Array.from(praseLine(line));
    switch(op) {
      case 'mask':
        mask = args[0];
        break;

      case 'mem':
        const result = floatingMask(mask, Number.parseInt(args[0]));
        processFloats([result]).forEach(addr => memoryMap.set(parseInt(addr, 2), args[1]))
        break;

      default:
        throw new Error('Invalid Operation');
    }
  });
  return Array.from(memoryMap.values());
}

function applyMask(mask: string, n: number): string {
  const baseBits = "000000000000000000000000000000000000";
  const binary = (n >>> 0).toString(2);
  const fullString = baseBits.substr(binary.length) + binary;
  return fullString.split('').map((char, i) => {
    const maskChar = mask[i];
    return maskChar === 'X' ? char : maskChar;
  }).join('');
}

function floatingMask(mask: string, n: number): string {
  const baseBits = "000000000000000000000000000000000000";
  const binary = (n >>> 0).toString(2);
  const fullString = baseBits.substr(binary.length) + binary;
  return fullString.split('').map((char, i) => {
    const maskChar = mask[i];
    return maskChar === '0' ? char : maskChar;
  }).join('');
}

function praseLine(line: string): RegExpMatchArray {
  const maskRegex = /^(mask) = ([X01]{36})$/;
  const memRegex = /^(mem)\[(\d+)] = (\d+)$/;
  const captureGroup = line.match(maskRegex) === null ? line.match(memRegex) : line.match(maskRegex);
  if (captureGroup === null)
    throw new Error('Could not parse line');

  return captureGroup;
}

function processFloats(floats: string[]): string[] {
  const currentFloats: string[] = [];
  floats.forEach(float => {
    const zero: string = float.replace('X', '0');
    const one: string = float.replace('X', '1');
    if (zero !== float || one !== float)
      currentFloats.push(zero, one);
  });
  if (currentFloats.length === 0)
    return floats;

  return processFloats(currentFloats);
}

if (import.meta.main) {
  const text = await readInput('14');
  const input = text.split("\n")
    .filter((line) => line !== "");
  const part1 = initializeProgram(input);
  console.log(`Part 1 : ${part1.map(n => parseInt(n, 2))
    .reduce((acc, curr) => acc + curr, 0)}`);
  const part2 = initializeWithFloats(input);
  console.log(`Part 2 : ${part2.map(n => Number.parseInt(n))
    .reduce((acc, curr) => acc + curr, 0)}`);
}